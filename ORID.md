## O
- During today's code review, we worked on one classmate's code of the command pattern implementation trying to make it better.
- Don't modify the private properties of an object directly externally.
- The behavior of an object should not be lost when using the command pattern.
- A method should not exceed 15 lines.
- We list cases based on 4 stories of parking lot and then write test cases and then implement the feature.
- We wrote a lot of code also shared our classmates' code on the screen.
## R
- When I was asked to change my code while sharing the screen, I was really nerve-wracking.
## I
- Design patterns should not be applied rigidly, otherwise it will only increase the dependency of the code.
- I learn a lot and reflect on myself every time I do a code review.
- My test case's name is becoming longer and longer.
## D
- I need to learn more about design patterns.

