package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private final Map<ParkingTicket, Car> parkedCars = new HashMap<>();
    private final int parkCapacity;

    public ParkingLot(int parkCapacity) {
        this.parkCapacity = parkCapacity;
    }

    public ParkingTicket park(Car car) {
        if (this.isFull()) {
            throw new NoAvailablePositionException("No available position");
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        this.parkedCars.put(parkingTicket, car);
        return parkingTicket;
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (isTicketInvalid(parkingTicket)) {
            throw new UnrecognizedParkingTicketException("Unrecognized parking ticket");
        }
        return this.parkedCars.remove(parkingTicket);
    }

    public boolean isFull() {
        return this.parkedCars.size() >= this.parkCapacity;
    }

    public boolean isTicketInvalid(ParkingTicket parkingTicket) {
        return this.parkedCars.get(parkingTicket) == null;
    }

    public boolean containsCar(ParkingTicket parkingTicket) {
        return this.parkedCars.containsKey(parkingTicket);
    }

    public int vacantParkingSpace(){
        return parkCapacity - parkedCars.size();
    };

    public double calculateAvailablePositionRate() {
        return (double) this.vacantParkingSpace() / this.parkCapacity;
    }
}
