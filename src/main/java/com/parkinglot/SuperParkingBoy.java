package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SuperParkingBoy extends ParkingBoy {
    public SuperParkingBoy(ParkingLot... parkingLots) {
        this.parkingLots = Arrays.asList(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car) {
        return this.parkingLots
                .stream()
                .max(Comparator.comparing(ParkingLot::calculateAvailablePositionRate))
                .orElseThrow(() -> new NoAvailablePositionException("No available position"))
                .park(car);
    }
}
