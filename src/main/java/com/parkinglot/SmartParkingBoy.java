package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SmartParkingBoy extends ParkingBoy {

    public SmartParkingBoy(ParkingLot... parkingLots) {
        this.parkingLots = Arrays.asList(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car) {
        return this.parkingLots
                .stream()
                .max(Comparator.comparing(ParkingLot::vacantParkingSpace))
                .orElseThrow(() -> new NoAvailablePositionException("No available position"))
                .park(car);
    }
}
