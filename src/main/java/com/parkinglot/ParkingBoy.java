package com.parkinglot;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class ParkingBoy {

    protected List<ParkingLot> parkingLots;
    protected abstract ParkingTicket park(Car car);

    public ParkingBoy(ParkingLot... parkingLots) {
        this.parkingLots = Arrays.asList(parkingLots);
    }

    public Car fetch(ParkingTicket parkingTicket) {
        return this.parkingLots
                .stream()
                .filter(parkingLot->parkingLot.containsCar(parkingTicket))
                .findFirst()
                .orElseThrow(() -> new UnrecognizedParkingTicketException("Unrecognized parking ticket"))
                .fetch(parkingTicket);
    }
}
