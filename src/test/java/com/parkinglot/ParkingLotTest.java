package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingLotTest {
    @Test
    void return_a_parking_ticket_when_park_the_car_given_a_car_and_a_parking_lot() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);

        ParkingTicket parkingTicket = parkingLot.park(car);

        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void return_a_parked_car_when_fetch_the_car_given_a_ticket_and_a_parking_lot_parked_a_car() {
        Car carParked = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingTicket parkingTicket = parkingLot.park(carParked);

        Car carFetched = parkingLot.fetch(parkingTicket);

        Assertions.assertEquals(carParked, carFetched);
    }

    @Test
    void return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_tickets_and_a_parking_lot_parked_two_cars() {
        Car carParked1 = new Car();
        Car carParked2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingTicket parkingTicket1 = parkingLot.park(carParked1);
        ParkingTicket parkingTicket2 = parkingLot.park(carParked2);

        Car carFetched1 = parkingLot.fetch(parkingTicket1);
        Car carFetched2 = parkingLot.fetch(parkingTicket2);

        Assertions.assertEquals(carParked1, carFetched1);
        Assertions.assertEquals(carParked2, carFetched2);
        Assertions.assertNotEquals(carFetched1, carFetched2);
    }

    @Test
    void return_unrecognized_parking_ticket_when_fetch_the_car_given_wrong_ticket_and_a_parking_lot_parked_a_car() {
        Car carParked = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        parkingLot.park(carParked);
        ParkingTicket wrongTicket = new ParkingTicket();

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void return_unrecognized_parking_ticket_when_fetch_the_car_given_used_ticket_and_a_parking_lot_parked_a_car() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingTicket usedTicket = parkingLot.park(car);
        parkingLot.fetch(usedTicket);

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(usedTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void return_wrong_message_no_available_position_when_park_the_car_given_a_car_and_a_full_parking_lot() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        for (int i = 0; i < 10; i++) {
            Car carParked = new Car();
            parkingLot.park(carParked);
        }

        Exception exception = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car));

        assertEquals("No available position", exception.getMessage());
    }

}
