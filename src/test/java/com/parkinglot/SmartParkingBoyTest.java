package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {
    @Test
    void should_park_in_parking_lot_contains_more_empty_positions_when_park_the_car_given_a_car_and_a_smart_parking_boy_manage_two_parking_lots() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);

        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        Assertions.assertNotNull(parkingLot2.fetch(parkingTicket));
    }

    @Test
    void should_park_in_first_parking_lot_when_park_the_car_given_a_car_and_a_smart_parking_boy_manage_two_parking_lots_with_same_empty_position() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);

        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        Assertions.assertNotNull(parkingLot1.fetch(parkingTicket));
    }

    @Test
    void should_park_in_parking_lot_contains_more_empty_positions_when_park_the_car_given_a_car_and_a_smart_parking_boy_manage_one_available_parking_lot_and_one_full_parking_lot() {
        Car carParked = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLot1.park(new Car());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket parkingTicket = smartParkingBoy.park(carParked);

        Car carFetched = parkingLot2.fetch(parkingTicket);

        Assertions.assertEquals(carParked, carFetched);
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_tickets_from_different_parking_lot_and_a_smart_parking_boy_manage_two_parking_lots_each_parked_with_one_car() {
        Car carParked1 = new Car();
        Car carParked2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket parkingTicket1 = parkingLot1.park(carParked1);
        ParkingTicket parkingTicket2 = parkingLot2.park(carParked2);

        Car carFetched1 = smartParkingBoy.fetch(parkingTicket1);
        Car carFetched2 = smartParkingBoy.fetch(parkingTicket2);

        Assertions.assertEquals(carFetched1, carParked1);
        Assertions.assertEquals(carFetched2, carParked2);
        Assertions.assertNotEquals(carFetched1, carFetched2);
    }

    @Test
    void should_throw_message_unrecognized_parking_ticket_when_fetch_the_car_given_wrong_ticket_and_a_smart_parking_boy_manage_two_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket wrongTicket = new ParkingTicket();

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(wrongTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_message_unrecognized_parking_ticket_when_fetch_the_car_given_used_ticket_and_a_smart_parking_boy_manage_two_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket usedTicket = smartParkingBoy.park(new Car());
        smartParkingBoy.fetch(usedTicket);

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> smartParkingBoy.fetch(usedTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_message_no_available_position_when_park_the_car_given_a_car_and_a_smart_parking_boy_manage_two_full_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        smartParkingBoy.park(new Car());
        smartParkingBoy.park(new Car());

        Exception exception = assertThrows(NoAvailablePositionException.class, () -> smartParkingBoy.park(new Car()));

        assertEquals("No available position", exception.getMessage());
    }
}
