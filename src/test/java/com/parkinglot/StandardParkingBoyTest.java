package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StandardParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_the_car_given_a_car_and_a_parking_lot_and_a_parking_boy() {
        Car car = new Car();
        ParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));

        System.out.println(standardParkingBoy.parkingLots);
        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        Assertions.assertNotNull(parkingTicket);
    }

    @Test
    void should_return_a_parked_car_when_fetch_the_car_given_a_ticket_and_a_parking_boy_and_a_parking_lot_parked_a_car() {
        Car carParked = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        ParkingTicket parkingTicket = standardParkingBoy.park(carParked);

        Car carFetched = standardParkingBoy.fetch(parkingTicket);

        Assertions.assertEquals(carParked, carFetched);
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_tickets_and_a_parking_lot_parked_two_cars_and_a_parking_boy() {
        Car carParked1 = new Car();
        Car carParked2 = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        ParkingTicket parkingTicket1 = standardParkingBoy.park(carParked1);
        ParkingTicket parkingTicket2 = standardParkingBoy.park(carParked2);

        Car carFetched1 = standardParkingBoy.fetch(parkingTicket1);
        Car carFetched2 = standardParkingBoy.fetch(parkingTicket2);

        Assertions.assertEquals(carParked1, carFetched1);
        Assertions.assertEquals(carParked2, carFetched2);
        Assertions.assertNotEquals(carParked1, carFetched2);
    }

    @Test
    void should_return_unrecognized_parking_ticket_when_fetch_the_car_given_used_ticket_and_a_parking_lot_parked_a_car_and_a_parking_boy() {
        Car car = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        ParkingTicket usedTicket = standardParkingBoy.park(car);
        standardParkingBoy.fetch(usedTicket);

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(usedTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_unrecognized_parking_ticket_when_fetch_the_car_given_wrong_ticket_and_a_parking_lot_parked_a_car_and_a_parking_boy() {
        Car carParked = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        ParkingTicket wrongTicket = new ParkingTicket();

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(wrongTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_wrong_message_no_available_position_when_park_the_car_given_a_car_and_a_full_parking_lot_and_a_parking_boy() {
        Car car = new Car();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(new ParkingLot(10));
        for (int i = 0; i < 10; i++) {
            Car carParked = new Car();
            standardParkingBoy.park(carParked);
        }

        Exception exception = assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(car));

        assertEquals("No available position", exception.getMessage());
    }
    @Test
    void should_park_in_first_parking_lot_when_park_the_car_given_a_car_and_a_standard_parking_boy_manage_two_parking_lots() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);

        ParkingTicket parkingTicket = standardParkingBoy.park(car);

        Assertions.assertNotNull(parkingLot1.fetch(parkingTicket));
    }

    @Test
    void should_park_in_second_parking_lot_when_park_the_car_given_a_car_and_a_standard_parking_boy_manage_two_parking_lots_with_first_parking_lot_full() {
        Car carParked = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        parkingLot1.park(new Car());
        ParkingTicket parkingTicket = standardParkingBoy.park(carParked);

        Car carFetched = parkingLot2.fetch(parkingTicket);

        Assertions.assertEquals(carParked,carFetched);
    }
    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_tickets_from_different_parking_lot_and_a_parking_boy_manage_two_parking_lots_each_parked_with_one_car() {
        Car carParked1 = new Car();
        Car carParked2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket parkingTicket1 = parkingLot1.park(carParked1);
        ParkingTicket parkingTicket2 = parkingLot2.park(carParked2);

        Car carFetched1 = standardParkingBoy.fetch(parkingTicket1);
        Car carFetched2 = standardParkingBoy.fetch(parkingTicket2);

        Assertions.assertEquals(carFetched1,carParked1);
        Assertions.assertEquals(carFetched2,carParked2);
        Assertions.assertNotEquals(carFetched1,carFetched2);
    }
    @Test
    void should_throw_message_unrecognized_parking_ticket_when_fetch_the_car_given_wrong_ticket_and_a_parking_boy_manage_two_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket wrongTicket = new ParkingTicket();

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(wrongTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_message_unrecognized_parking_ticket_when_fetch_the_car_given_used_ticket_and_a_parking_boy_manage_two_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket usedTicket = standardParkingBoy.park(new Car());
        standardParkingBoy.fetch(usedTicket);

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> standardParkingBoy.fetch(usedTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }
    @Test
    void should_throw_message_no_available_position_when_park_the_car_given_a_car_and_a_parking_boy_manage_two_full_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLot1, parkingLot2);
        standardParkingBoy.park(new Car());
        standardParkingBoy.park(new Car());

        Exception exception = assertThrows(NoAvailablePositionException.class, () -> standardParkingBoy.park(new Car()));

        assertEquals("No available position", exception.getMessage());
    }
}
