package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperParkingBoyTest {
    @Test
    void should_park_in_parking_lot_has_larger_available_position_rate_when_park_the_car_given_a_car_and_a_super_parking_boy_manage_two_parking_lots() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        parkingLot2.park(new Car());

        ParkingTicket parkingTicket = superParkingBoy.park(car);

        Assertions.assertNotNull(parkingLot1.fetch(parkingTicket));
    }

    @Test
    void should_park_in_first_parking_lot_when_park_the_car_given_a_car_and_a_super_parking_boy_manage_two_parking_lots_with_same_available_position_rate() {
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);

        ParkingTicket parkingTicket = superParkingBoy.park(car);

        Assertions.assertNotNull(parkingLot1.fetch(parkingTicket));
    }

    @Test
    void should_park_in_parking_lot_has_larger_available_position_rate_when_park_the_car_given_a_car_and_a_super_parking_boy_manage_one_available_parking_lot_and_one_full_parking_lot() {
        Car carParked = new Car();
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        parkingLot1.park(new Car());
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket parkingTicket = superParkingBoy.park(carParked);

        Car carFetched = parkingLot2.fetch(parkingTicket);

        Assertions.assertEquals(carParked, carFetched);
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_tickets_from_different_parking_lot_and_a_super_parking_boy_manage_two_parking_lots_each_parked_with_one_car() {
        Car carParked1 = new Car();
        Car carParked2 = new Car();
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket parkingTicket1 = superParkingBoy.park(carParked1);
        ParkingTicket parkingTicket2 = superParkingBoy.park(carParked2);

        Car carFetched1 = superParkingBoy.fetch(parkingTicket1);
        Car carFetched2 = superParkingBoy.fetch(parkingTicket2);

        Assertions.assertEquals(carFetched1, carParked1);
        Assertions.assertEquals(carFetched2, carParked2);
        Assertions.assertNotEquals(carFetched1, carFetched2);
    }

    @Test
    void should_throw_message_unrecognized_parking_ticket_when_fetch_the_car_given_wrong_ticket_and_a_super_parking_boy_manage_two_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket wrongTicket = new ParkingTicket();

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> superParkingBoy.fetch(wrongTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_message_unrecognized_parking_ticket_when_fetch_the_car_given_used_ticket_and_a_super_parking_boy_manage_two_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket usedTicket = superParkingBoy.park(new Car());
        superParkingBoy.fetch(usedTicket);

        Exception exception = assertThrows(UnrecognizedParkingTicketException.class, () -> superParkingBoy.fetch(usedTicket));

        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_throw_message_no_available_position_when_park_the_car_given_a_car_and_a_super_parking_boy_manage_two_full_parking_lots() {
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        SuperParkingBoy superParkingBoy = new SuperParkingBoy(parkingLot1, parkingLot2);
        superParkingBoy.park(new Car());
        superParkingBoy.park(new Car());

        Exception exception = assertThrows(NoAvailablePositionException.class, () -> superParkingBoy.park(new Car()));

        assertEquals("No available position", exception.getMessage());
    }
}
